#!/bin/bash

CONFIG_FILE="/src/statsd/config.js"

if [ "${GRAPHITE_HOST}" != "" ]; then
    sed -i "s/graphiteHost: \"127.0.0.1\"/graphiteHost: \"${GRAPHITE_HOST}\"/g" "${CONFIG_FILE}"
fi

if [ "${GRAPHITE_PORT}" != "" ]; then
    sed -i "s/graphitePort: 2003/graphitePort: ${GRAPHITE_PORT}/g" "${CONFIG_FILE}"
fi